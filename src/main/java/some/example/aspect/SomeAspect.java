package some.example.aspect;

import javax.annotation.PostConstruct;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SomeAspect {
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @PostConstruct
    public void init() {
        log.warn("postconstruct");
    }
    
    @Pointcut("execution(public * *(..))")
    private void anyPublicOperation() {}

    @Before("anyPublicOperation()")
    public void logBefore() {
        log.info("@Before");
    }
}
